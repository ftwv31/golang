package main

import (
	"flag"
	"fmt"
	"math/rand"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"

	"github.com/golang/glog"
	"github.com/prometheus/client_golang/prometheus/promhttp"

	"gitee.com/ftwv31/golang/homeworke4/metrics"
)

func main() {
	flag.Set("v", "4")
	glog.V(2).Info("Starting http server...")
	metrics.Register()

	http.HandleFunc("/", rootHandler)
	http.HandleFunc("/healthz", healthzHandler)
	http.HandleFunc("/readEnv", readEnvHander)
	http.HandleFunc("/log", logHander)
	http.Handle("/metrics", promhttp.Handler())

	error := http.ListenAndServe(":8080", nil)
	if error != nil {
		glog.Fatalln(error)
	}
}

/**
curl -v host:8080/
*/
func rootHandler(w http.ResponseWriter, r *http.Request) {
	metrics.CountPath("/")
	timer := metrics.NewTimer()
	defer timer.ObserveTotal()
	delay := randInt(10, 2000)
	time.Sleep(time.Millisecond * time.Duration(delay))
	for k, v := range r.Header {
		fmt.Println(k, "=", v)
		fmt.Fprintf(w, "%s=%s\n", k, v)
	}
}

func randInt(min int, max int) int {
	rand.Seed(time.Now().UTC().UnixNano())
	return min + rand.Intn(max-min)
}

/**
curl -v host:8080/healthz
*/
func healthzHandler(w http.ResponseWriter, r *http.Request) {
	metrics.CountPath("/healthz")

	fmt.Fprintf(w, "ok\n")
}

/**
curl -v host:8080/readEnv?env=XXXX
*/
func readEnvHander(w http.ResponseWriter, r *http.Request) {
	metrics.CountPath("/readEnv")

	queryForm, error := url.ParseQuery(r.URL.RawQuery)
	if error == nil && len(queryForm["env"]) > 0 {
		goRoot := os.Getenv(queryForm["env"][0])
		if len(goRoot) < 1 {
			fmt.Fprintf(w, "未找到%s 对应的环境变量值。\n", queryForm["env"][0])
		} else {
			//设置header顺序问题，在写内容前生效，反之不生效
			w.Header().Set(queryForm["env"][0], goRoot)
			fmt.Fprintf(w, "goRoot=%s\n", goRoot)
		}
	} else {
		fmt.Fprintf(w, "请使用/readEnv?env=KEY,的方式\n")
	}
}

/**
curl -v host:8080/log
*/
func logHander(w http.ResponseWriter, r *http.Request) {
	metrics.CountPath("/log")

	// ip := r.RemoteAddr
	forwarded := r.Header.Get("X-FORWARDED-FOR")
	if forwarded == "" {
		forwarded = r.RemoteAddr
	}
	code := w.Header().Get("status")
	fmt.Fprintf(w, "ip:%s,code:%s", strings.Split(forwarded, ":")[0], code)
}
