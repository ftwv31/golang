## 安装jaeger

`
kubectl apply -f jaeger.yaml
kubectl edit configmap istio -n istio-system
#设置采样率为100%
set tracing.sampling=100
`

## 部署jaeger
·
kubectl create ns tracing
kubectl label ns tracing istio-injection=enabled
kubectl -n tracing apply -f service0.yaml
kubectl -n tracing apply -f service1.yaml
kubectl -n tracing apply -f service2.yaml 
kubectl apply -f istio-tracing.yaml -n tracing
`

## 获取ingress的IP

`
kubectl get svc -nistio-system
istio-ingressgateway   LoadBalancer   $INGRESS_IP
`

## 访问 jaeger管理台界面

`
istioctl dashboard jaeger
`

## 修改main.go 支持tracing

- 小写获取Header信息，并传递到后端服务
- tracing目录下为相关描述文件
