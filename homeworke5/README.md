## webserver部署

`
kubectl create ns securesvc
kubectl label ns securesvc istio-injection=enabled
kubectl create -f deployment.yaml -n securesvc
kubectl create -f service.yaml -n securesvc

## 生成证书（使用老师已生成的证书）
·
openssl req -x509 -sha256 -nodes -days 365 -newkey rsa:2048 -subj '/O=cncamp Inc./CN=*.cncamp.io' -keyout cncamp.io.key -out cncamp.io.crt
kubectl create -n istio-system secret tls cncamp-credential --key=cncamp.io.key --cert=cncamp.io.crt
kubectl apply -f istio-https.yaml -n securesvc
`

## 获取ingress的IP

`
kubectl get svc -nistio-system
istio-ingressgateway   LoadBalancer   $INGRESS_IP
`

## 访问 webserver

`
curl --resolve ftwc31.cncamp.io:443:$INGRESS_IP https://ftwc31.cncamp.io/healthz -v -k
`

## 修改main.go 支持tracing

- 小写获取Header信息，并传递到后端服务
- tracing目录下为相关描述文件
