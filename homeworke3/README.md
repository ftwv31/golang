## webserver使用 homeworke2的代码

- 优雅停止（未完成）

## depoloyment.yaml 为webserver的部署文件

- 包含livenessProbe、readinessProbe 探活
- 默认为2个副本文件，部署采用滚动升级策略
- 采用了podAntiAffinity的亲和度配置，使用软策略，每个节点只有一个pod，如果不满足条件也可以一个节点多个POD
- 环境变量采用configMap配置文件进行配置，可以通过curl localhost:8080/readEnv?env=ENV_CONFIG读取
- 采用resources配置了简单的资源需求

## service.yaml 为webserver的服务描述文件

- 采用ClusterIp类型

## ingress.yaml与secret.yaml 为ingress的配置

- 采用了tls证书管理（证书为老师教材提供的内容）
- 访问采用强制https访问配置，如果输入的是http会自动转到https

