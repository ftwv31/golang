## homeworke1

编写一个 HTTP 服务器，大家视个人不同情况决定完成到哪个环节，但尽量把 1 都做完。

接收客户端 request，并将 request 中带的 header 写入 response header
读取当前系统的环境变量中的 VERSION 配置，并写入 response header
Server 端记录访问日志包括客户端 IP，HTTP 返回码，输出到 server 端的标准输出
当访问 localhost/healthz 时，应返回 200

## homeworke2

构建本地镜像。
编写 Dockerfile 将练习 2.2 编写的 httpserver 容器化（请思考有哪些最佳实践可以引入到 Dockerfile 中来）。
将镜像推送至 Docker 官方镜像仓库。
通过 Docker 命令本地启动 httpserver。
通过 nsenter 进入容器查看 IP 配置。

## homeworke3

编写 Kubernetes 部署脚本将 httpserver 部署到 kubernetes 集群，以下是你可以思考的维度

优雅启动
优雅终止
资源需求和 QoS 保证
探活
日常运维需求，日志等级
配置和代码分离

来尝试用 Service, Ingress 将你的服务发布给集群外部的调用方吧
在第一部分的基础上提供更加完备的部署 spec，包括（不限于）

Service
Ingress
可以考虑的细节

如何确保整个应用的高可用
如何通过证书保证 httpServer 的通讯安全


## homeworke4

为 HTTPServer 添加 0-2 秒的随机延时

为 HTTPServer 项目添加延时 Metric

为 HTTPServer 项目添加path访问资源 Metric (扩展)

将 HTTPServer 部署至测试集群，并完成 Prometheus 配置

从 Promethus 界面中查询延时指标数据

（可选）创建一个 Grafana Dashboard 展现延时分配情况

## homeworke5

将 httpserver 服务以 Istio Ingress Gateway 的形式发布出来。

以下是你需要考虑的几点：

如何实现安全保证；
七层路由规则；
考虑 open tracing 的接入。
